import random

adj_list = ['intrepid', 'effective', 'efficient', 'thoughtful', 'tactiful',
            'eloquent', 'big']

noun_list = ['python', 'programmer', 'coder', 'developer', 'snake', 'snek',
             'engineer', 'software']

group_list = ['body', 'party', 'organization', 'force', 'contingent', 'corps'
              'army', 'company', 'posse', 'entourage', 'crew', 'band',
              'detachment', 'troop', 'team', 'group', 'outfit', 'squad', 'unit'
              'consortium', 'contingent', 'collective', 'brigade']

program_running = True
generate = ""

while program_running:
    generate = input(
        'Type in the letter "g" to generate a cool programming group name! ')

    if generate.lower() == "g":
        random_adj = adj_list[random.randint(0, (len(adj_list)-1))]
        random_noun = noun_list[random.randint(0, (len(noun_list)-1))]
        random_group = group_list[random.randint(0, (len(group_list)-1))]
        print(
            f"Your group is the {random_adj}_{random_noun}_{random_group}"
        )
        break
    else:
        print("Please input a valid key.")
